let peopleSalary = require('./homework2.js');

for (let arrayIndex in peopleSalary) {
  for (let key in peopleSalary[arrayIndex]) {
    if (key === 'salary') {
      let salary = peopleSalary[arrayIndex][key];
      peopleSalary[arrayIndex][key] = [
        salary,
        parseInt(salary * 1.1),
        parseInt(salary * 1.1 * 1.1),
      ];
    }
  }
}

console.log(peopleSalary);
