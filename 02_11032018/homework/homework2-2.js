async function getHomework1Json() {
  const response = await fetch('homework1.json');
  const peopleSalary = await response.json();
  //console.log(peopleSalary);
  const peopleLowSalary = [...peopleSalary]
    .filter(row => {
      return parseInt(row.salary) < 100000;
    })
    .map(row => {
      console.log(parseInt(row.salary));
      row.salary = parseFloat(row.salary) * 2;
      return row;
    });
  console.log('homework2-2');
  console.log(peopleLowSalary);
  const sumSalary = peopleLowSalary.reduce((sum, row) => {
    return sum + parseInt(row.salary);
  }, 0);
  console.log(sumSalary);
}
getHomework1Json();
