async function getHomework14Json() {
  const response = await fetch('homework1-4.json');
  const datas = await response.json();
  //console.log(peopleSalary);
  const datas1 = [...datas]
    .filter(row => {
      return row.gender === 'male' && row.friends.length >= 2;
    })
    .map(row => {
      return {
        name: row.name,
        gender: row.gender,
        company: row.company,
        email: row.email,
        friends: row.friends,
        balance: row.balance,
      };
    });
  console.log('homework2-3');
  console.log(datas1);

  datas2 = datas.map(row => {
    row.balance =
      '$' +
      parseFloat(
        parseFloat(row.balance.replace(',', '').replace('$', '')) / 10
      ).toFixed(2);
    return row;
  });
  console.log(datas2);
}

getHomework14Json();
